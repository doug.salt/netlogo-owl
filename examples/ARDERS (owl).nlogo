extensions [ owl ]

breed [ villages village ]

breed [ targets target ]

breed [ extorters extorter ]

breed [ police police-officer ]

breed [ public consumer ]

globals [
  ERROR-OCCURRED?
  confiscated
  total-money
  shopping-amount
  max-shop-distance
  max-customers
  interface-filename
  max-family-depth
  this-family-depth
  size-of-largest-family
  time-when-addio-pizzo-takes-over
  m-ass-a-p-shops
  m-ass-non-a-p
  m-ass-ext-act
  m-ass-ext-jail
  m-ass-ext-inact
]

targets-own [
  shop-wealth
  old-wealth
  monthly-income
  addio-pizzo?
  bankrupt?
  current-customers
  date-of-recent-punishment
  amount-of-recent-punishment
]

extorters-own [
  extortion-radius
  extortion-level
  punishment-severity
  extorter-type
  extortion-now
  wealth
  forbidden-targets
  active?
  in-jail?
  was-in-jail?
  time-of-jailing
  time-of-deactivation
  reactivation-time
  my-family
  role
  n-of-family-members
]

public-own [
  my-current-shop
  distance-to-my-current-shop
  private-wealth
  consumption-rate
]
patches-own [

]

directed-link-breed
[
  reports-to-links reports-to-link
]

directed-link-breed
[
  levies-tribute-from-links levies-tribute-from-link
]

directed-link-breed
[
  pay-links pay-link
]


directed-link-breed
[
  threaten-links threaten-link
]

pay-links-own
[

]

threaten-links-own [

]


;;
;; Setup Procedures
;;

to setup

  ;; (for this model to work with NetLogo's new plotting features,
  ;; __clear-all-and-reset-ticks should be replaced with clear-all at
  ;; the beginning of your setup procedure and reset-ticks at the end
  ;; of the procedure.)
  clear-all
  clear-output
  set interface-filename ( word "result_" rectify-date "_d" ( denunciation-propensity * 100 ) "_p" ( prosecution-propensity * 100 ) ".png" )
  set time-when-addio-pizzo-takes-over -1
  set ERROR-OCCURRED? false
  set total-money 0
  if batch-version?
  [
    set debug? false
    set random-seed? false
    set denunciation-propensity random-float-in-range 0.1 0.7
    set prosecution-propensity random-float-in-range 0.1 0.7
    set extortion-level-low random-float-in-range 10 50
    set extortion-level-high random-float-in-range extortion-level-low 75
    set punishment-severity-low random-float-in-range 10 50
    set punishment-severity-high random-float-in-range punishment-severity-low 75
    set addio-pizzo-threshold random-float-in-range 0 0.5
  ]
  if debug? [ file-open ( word "logfile" rectify-date  ".txt" ) ]
  if debug? [ file-show ( list "Output from " date-and-time ) ]
  ; If set to ON, the same random values are used. In this case 4711.
  if random-seed?
  [ random-seed 4711 ]

  create-villages initial-villages [ villages-setup ]
  setup-patches
  create-police initial-police [ police-setup ]
  create-targets initial-targets [ targets-setup ]
  set m-ass-non-a-p mean [ shop-wealth ] of targets
  create-extorters initial-extorters [ extorters-setup ]
  set m-ass-ext-act mean [ wealth ] of extorters
  create-public initial-public [ public-setup ]
  set confiscated 0
  set shopping-amount initial-wealth
  set max-customers 2 * initial-public / initial-targets

  my-update-plots
  reset-ticks
end

to villages-setup
  set color one-of [ 15 25 35 45 55 65 76 85 95 105 115 125 135 ]
  set shape "church"
  set size 3
  move-to one-of patches with [ not any? other turtles-here ]
end

to public-setup
  set color white
  set shape "person"
  set size 0.5
  move-to one-of patches with [ not any? other turtles-here ]
  set my-current-shop min-one-of targets [ distance myself ]
  set private-wealth initial-wealth
  set consumption-rate random-float-in-range consumption-rate-low consumption-rate-high
end

to police-setup
  set color green
  set shape "police"
  move-to one-of villages
  move-to one-of patches in-radius 2 with [not any? other turtles-here]
end

to targets-setup ;; targets procedure
  set color blue
  set shape "house"
  move-to one-of villages
  move-to one-of patches in-radius ( world-width / sqrt count villages ) with [ not any? other turtles-here ]
  set addio-pizzo? false
  set bankrupt? false
  set shop-wealth random-float-in-range initial-wealth-min initial-wealth-max
  set old-wealth shop-wealth
end

to extorters-setup
  set extortion-radius initial-extortion-radius
  set extorter-type "LL"
  set extortion-level extortion-level-low
  if ( random-float  1.0 > 0.5 )
  [
    set extortion-level extortion-level-high
    set extorter-type replace-item 0 extorter-type "H"
  ]
  set extortion-now extortion-level
  set punishment-severity punishment-severity-low
  if ( random-float  1.0 > 0.5 )
  [
    set punishment-severity punishment-severity-high
    set extorter-type replace-item 1  extorter-type "H"
  ]
  set reactivation-time random-float-in-range reactivation-time-low reactivation-time-high
  color-extorter
  set shape "person"
  set wealth 2000
  set forbidden-targets []
  set my-family who
  set role "*"
  set active? true
  set in-jail? false
  set was-in-jail? false
  move-to one-of villages
  move-to one-of patches in-radius 3 with [ not any? other turtles-here ]

end

to color-extorter
   set color 45
  if extorter-type = "LH" [ set color 25 ]
  if extorter-type = "HL" [ set color 15 ]
  if extorter-type = "HH" [ set color 125 ]
end

to setup-patches
  ask patches
  [
    let nearest-village min-one-of villages [ distance myself ]
    set pcolor 3 + [ color ] of nearest-village
  ]
end

;;
;; Runtime Procedures
;;
to go
  if debug? [ file-show sort [ count out-pay-link-neighbors ] of targets ]

  ask targets [ set old-wealth shop-wealth ]

  ask public
  [
    go-shopping
  ]
  if not batch-version? [ set max-shop-distance max [ distance-to-my-current-shop ] of public ]

  ask targets
  [
    set monthly-income shop-wealth - old-wealth
  ]

  ask extorters
  [
    if active? and not in-jail?
    [
      if count my-in-pay-links = 0 or any? targets with [ count my-in-threaten-links = 0 ]
      [
        if not was-in-jail? [ set extortion-radius extortion-radius-extension * extortion-radius ]
        find-victims
      ]
    ]
    if active? and wealth <= 0
    [
      ask out-threaten-link-neighbors [ ask myself [ give-up self ] ]
      set active? false
      set color white
      set total-money total-money + wealth
      set wealth 0
      set time-of-deactivation ticks
    ]
    if in-jail?
    [
      if ticks > time-of-jailing + time-in-jail
      [
        set in-jail? false
        set was-in-jail? true
        color-extorter
        set extortion-radius initial-extortion-radius * 0.5
      ]
    ]
    if not active? and ticks - time-of-deactivation > reactivation-time
    [
      set active? true
      find-victims
      color-extorter
    ]
  ]

  ask targets with [ not bankrupt? ]
  [
;;    buy supply for inventory and pay salaries
    let expenses-per-period fixed-cost-per-period + variable-cost-coefficient * monthly-income
    set shop-wealth shop-wealth - expenses-per-period
    set total-money total-money + expenses-per-period
;;    earn-money
    if count my-out-pay-links > 0
    [
      ifelse random-float 1.0 < denunciation-propensity or addio-pizzo?
      [
        denounce
        if not addio-pizzo? [ declare-membership-in-addio-pizzo ]
      ]
      [
        pay-extortion
      ]
    ]
  ]

  ask targets with [ shop-wealth <= 0]
  [
    ask out-pay-link-neighbors [ give-up myself ]
    set total-money total-money + shop-wealth
    set shop-wealth 0
    set bankrupt? true
    set color black
  ]

  ask extorters
  [
    if active? and not in-jail?
    [
      let c consumption-per-period
      if  c > wealth [ set c wealth ]
      set wealth wealth - c
      set total-money total-money + c
    ]
  ]

  set shopping-amount total-money / initial-public
  ask public [ set private-wealth private-wealth + shopping-amount ]
  set total-money 0

  compensate

  report-hierarchies

;; preparing for output

  ifelse count targets with [ not addio-pizzo? and not bankrupt?] > 0
  [
    set m-ass-non-a-p mean ( [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ] )
  ]
  [
    set m-ass-non-a-p 0
  ]
  ifelse count targets with [ addio-pizzo? and not bankrupt? ] > 0
  [
    set m-ass-a-p-shops  mean ( [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ] )
  ]
  [
    set m-ass-a-p-shops 0
  ]
  if time-when-addio-pizzo-takes-over < 0
  [
    if m-ass-a-p-shops > m-ass-non-a-p
    [
      set time-when-addio-pizzo-takes-over ticks
    ]
  ]

  ifelse count extorters with [ active? ] > 0
  [
    set m-ass-ext-act mean [ wealth ] of extorters with [ active? ]
  ]
  [
    set m-ass-ext-act 0
  ]

  ifelse count extorters with [ in-jail? ] > 0
  [
    set m-ass-ext-jail mean [ wealth ] of extorters with [ in-jail? ]
  ]
  [
    set m-ass-ext-jail 0
  ]

  ifelse count extorters with [ not active? ] > 0
  [
    set m-ass-ext-inact mean [ wealth ] of extorters with [ not active? ]
  ]
  [
    set m-ass-ext-inact 0
  ]



  tick

;;  if ( not any? extorters with [ active? ] ) or ( ticks > 50 ) or ( not any? targets with [ not bankrupt? ] ) or ERROR-OCCURRED?
  if ticks > 50
  [
    if export-interface?
    [
      export-interface interface-filename
      show word "interface exported to " interface-filename
    ]
    save-structure "C:\\kgt\\Projects\\Current\\315874-GLODERS\\OWL\\owltest"
    stop
  ]
  my-update-plots
end

to compensate
  while [ confiscated > initial-wealth-max and any? targets with [ amount-of-recent-punishment > 0 ] ]
  [
    ask min-one-of ( targets with [ amount-of-recent-punishment > 0 ] ) [ date-of-recent-punishment ]
    [
      set confiscated confiscated - amount-of-recent-punishment
      set shop-wealth shop-wealth + amount-of-recent-punishment
      if debug? [ file-show ( list " was compensated with " amount-of-recent-punishment ) ]
      set amount-of-recent-punishment 0
      set date-of-recent-punishment ticks + 5
      if bankrupt?
      [
        set bankrupt? false
        set addio-pizzo? true
      ]
    ]
  ]
end

to report-sub-hierarchies [ indentation  ]
  ifelse count in-reports-to-link-neighbors = 0
  [
  ]
  [
    let followers in-reports-to-link-neighbors
    if debug?
    [
      output-type indentation
      if debug? [ file-type "followers: " ]
      output-print [ who ] of followers
      if debug? [ file-print [ who ] of followers ]
    ]
    set indentation word indentation "..."
    set this-family-depth this-family-depth + 1
    ask followers
    [
      if debug? [ file-show ( list "hierarchy below " who ) ]
      output-type indentation
      output-type who
      output-print role
      report-sub-hierarchies indentation
    ]
  ]
end

to report-hierarchies
  let top-positions extorters with [ ( count out-reports-to-link-neighbors = 0 ) and ( count in-reports-to-link-neighbors > 0 ) ]
  output-print "Current hierarchies"
  output-type "top-positions: "
  output-print [ who ] of top-positions
  let indentation "..."
  set this-family-depth 0
  set max-family-depth 0
  set size-of-largest-family 0
  ask top-positions
  [
    output-type indentation
    output-type who
    output-print role
    if debug? [ file-show ( list "hierarchy below " who ) ]
    report-sub-hierarchies indentation
  ]
  ask extorters
  [
    ifelse role = "*"
    [
      set size 2
      set n-of-family-members count extorters with [ family = [ my-family ] of myself ]
      if size-of-largest-family < n-of-family-members [ set size-of-largest-family n-of-family-members ]
    ]
    [ set size 1 ]
    if count out-reports-to-link-neighbors > 1
    [
      show family
      show ( list "ERROR: in  " [ who ] of in-reports-to-link-neighbors )
      show ( list "ERROR: out " [ who ] of out-reports-to-link-neighbors )
      set ERROR-OCCURRED? true
    ]
  ]
  if this-family-depth > max-family-depth [ set max-family-depth this-family-depth ]
end

to buy-from [ a-shop ]
  let my-shopping-amount consumption-rate * shopping-amount
  ask a-shop [ set shop-wealth shop-wealth + my-shopping-amount ]
  set private-wealth private-wealth - my-shopping-amount
end

to switch-to-addio-pizzo-shop
  let my-former-shop my-current-shop
  let potential-shops targets with [ addio-pizzo? and not bankrupt? ]
  if count potential-shops > 0 [ set my-current-shop min-one-of potential-shops [ current-customers ] ]
  if debug? and my-former-shop != my-current-shop [ file-show ( list " switches from " [ who ] of my-former-shop " to " [ who ] of my-current-shop ) ]
end

to go-shopping
  let my-former-shop my-current-shop
  ask my-current-shop [ set current-customers count public with [ my-current-shop = myself ] ]
  if [ bankrupt? or current-customers > max-customers ] of my-current-shop [ set my-current-shop nobody ]
  if my-current-shop = nobody
  [
    let active-targets targets with [ not bankrupt? ]
    set my-current-shop min-one-of active-targets [ current-customers ]
  ]
  if [ not addio-pizzo? ] of my-current-shop
  [
    if random-float 1.0 < addio-pizzo-threshold [ switch-to-addio-pizzo-shop ]
  ]
  if [ not bankrupt? ] of my-current-shop  [ buy-from my-current-shop ]
  if not batch-version?
  [
    set distance-to-my-current-shop distance my-current-shop
    set color scale-color black distance-to-my-current-shop max-shop-distance 1
  ]
  if debug? and my-former-shop != my-current-shop
  [
    if [ bankrupt? ] of my-former-shop [ file-show ( list " switches from " [ ( word who "/" current-customers ) ] of my-former-shop " to " [ ( word who "/" current-customers ) ] of my-current-shop " for bankruptcy " ) ]
    if [ current-customers > max-customers ] of my-former-shop [ file-show ( list " switches from " [ ( word who "/" current-customers ) ] of my-former-shop " to " [ ( word who "/" current-customers ) ] of my-current-shop " for crowding " ) ]
  ]
end

to declare-membership-in-addio-pizzo
  set addio-pizzo? true
  set color red
  if debug? [ file-show ( list " declared to be a supporter of addio pizzo " ) ]
end

to denounce
  let nearest-police-officer min-one-of police [ distance myself ]
  if debug? [ file-show ( list " asked " nearest-police-officer " to prosecute " [ who ] of out-pay-link-neighbors ) ]
  let my-extorters out-pay-link-neighbors
  ask nearest-police-officer [ prosecute my-extorters myself ]
end

to prosecute [ offenders denouncer ]
  if debug? [ file-show ( list " starts prosecuting" [ who ] of offenders ) ]
  ifelse random-float 1.0 < prosecution-propensity
  [
    ask offenders
    [
      set in-jail? true
      set confiscated confiscated + wealth * 0.9
      let remuneration wealth * 0.1
      ask denouncer [ set shop-wealth shop-wealth + remuneration ]
      set wealth 0
      set color grey
      set time-of-jailing ticks
      if debug? [ file-show ( list " deletes threaten-links to " out-threaten-link-neighbors ) ]
      ask my-out-threaten-links [ die ]
      if debug? [ file-show ( list " deletes pay-links from " in-pay-link-neighbors ) ]
      ask my-in-pay-links [ die ]
    ]
  ]
  [
    if debug? [ file-show " but gives up " ]
    relieve-from denouncer offenders
  ]
end

to relieve-from [ victim offenders ]
  ask offenders
  [
    punish victim
  ]
end

to punish [ victim ]
  let punishment [ shop-wealth ] of victim * punishment-severity / 100
  ask victim
  [
    if count out-pay-link-neighbors > 1 [ get-rid-of-other-extorters ]
    set shop-wealth shop-wealth - punishment
    set date-of-recent-punishment ticks
    set amount-of-recent-punishment punishment
    if debug? [ file-show ( list " is punished by " [ who ] of myself " for denouncing instead of paying " ) ]
  ]
  set wealth wealth + punishment
end

to get-rid-of-other-extorters
  let my-extorter max-one-of out-pay-link-neighbors [ punishment-severity ]
  if debug?
  [
    file-show [ who ] of out-pay-link-neighbors
    file-show [ extortion-now ] of out-pay-link-neighbors
  ]
  let other-extorters out-pay-link-neighbors
  while [ count out-pay-link-neighbors > 1 ]
  [
    if debug? [ file-show ( list " asks " my-extorter " to protect me against " [ who ] of other-extorters ) ]
    ask my-extorter [ protect-me-against myself other-extorters ]
    set other-extorters out-pay-link-neighbors
  ]
  if debug? [ file-show ( list " got rid of all extorters but " [ who ] of other-extorters ) ]
end

to pay-extortion
  let my-extorter max-one-of out-pay-link-neighbors [ punishment-severity ]
  let extortion-rate [ extortion-now ] of my-extorter
  if debug? and count out-pay-link-neighbors > 1
  [
    file-show [ who ] of out-pay-link-neighbors
    file-show [ extortion-now ] of out-pay-link-neighbors
  ]
  let extortion monthly-income * extortion-rate / 100
  if extortion > shop-wealth [ set extortion shop-wealth ]
  set shop-wealth shop-wealth - extortion
  ask my-extorter [ set wealth wealth + extortion ]
  if count out-pay-link-neighbors > 1
  [
    let other-extorters out-pay-link-neighbors
    while [ count out-pay-link-neighbors > 1 ]
    [
      if debug? [ file-show ( list " asks " my-extorter " to protect me against " [ who ] of other-extorters ) ]
      ask my-extorter [ protect-me-against myself other-extorters ]
      set other-extorters out-pay-link-neighbors
    ]
    if debug? [ file-show ( list " got rid of all extorters but " [ who ] of other-extorters ) ]
  ]
end

to-report family
  let result -1
  if count out-reports-to-link-neighbors > 1
  [
    set ERROR-OCCURRED? true
    show ( list "ERROR: family " [ who ] of out-reports-to-link-neighbors )
    ask out-reports-to-link-neighbors [ show ( list "ERROR: family " [ who ] of out-reports-to-link-neighbors ) ]
    report result
  ]
  ifelse count out-reports-to-link-neighbors = 0
  [ set result who ]
  [
    let boss one-of out-reports-to-link-neighbors
    ask boss [ set result family ]
  ]
  report result
end

to subordinate-yourself-to [ boss ]
  let boss-family 0
  set my-family family
  ask boss [ set boss-family family ]
  ifelse role = "*" and my-family != [ my-family ] of boss
  [
    create-reports-to-link-to boss [ set color 52 set shape "my-arc"]
    create-levies-tribute-from-link-from boss [ set color 25 set shape "my-arc" ]
    if debug? [ file-show ( list who " subordinated to " [ who ] of boss ) ]
    ifelse count in-reports-to-link-neighbors = 0 [ set role "-" ][ set role "+" ]
  ]
  [
    if debug? [ file-show (list who " did not subordinate to " [ who ] of boss ) ]
  ]
end

to protect-me-against [ protection-seeker rivals ]
  if not any? other rivals
  [
    stop
  ]
  set my-family family
  if my-family = -1 [ set ERROR-OCCURRED? true stop ]
  ask other rivals
  [
    give-up protection-seeker
    subordinate-yourself-to myself
  ]
end

to give-up [ your-target ]
  if debug? [ file-show ( list "is asked to give up " your-target ) ]
  ask my-in-pay-links with [ other-end = your-target ]
  [
    if debug? [ file-show ( list "will now die" ) ]
    die
  ]
  ask my-out-threaten-links with [ other-end = your-target ]
  [
    if debug? [ file-show ( list "will now die" ) ]
    die
  ]

  if debug?
  [
    file-show ( list " gave up target " [ who ] of your-target )
    file-show ( list " extorter's links " [ who ] of out-threaten-link-neighbors [ who ] of in-pay-link-neighbors )
    ask your-target [ file-show ( list " target's links " [ who ] of out-pay-link-neighbors [ who ] of in-threaten-link-neighbors ) ]
  ]
  set forbidden-targets lput [ who ] of your-target forbidden-targets
  if debug? and length forbidden-targets > 0 [ file-show ( list " will not extort " forbidden-targets ) ]
end

to find-victims
  let allowed-targets targets in-radius extortion-radius with [ not addio-pizzo? and not bankrupt? ] ;; and count my-out-pay-links = 0 ]
  foreach forbidden-targets
  [ ?1 ->
    set allowed-targets allowed-targets with [ who != ?1 ]
  ]

  if random-float 1.0 < propensity-to-offend-addio-pizzo and 0 < count targets in-radius extortion-radius with [ addio-pizzo? and not bankrupt? ] ;; and count my-out-pay-links = 0 ] > 0
  [
    let selected-anti-pizzo-target one-of  targets in-radius extortion-radius with [ addio-pizzo? and not bankrupt? ] ;; and count my-out-pay-links = 0 ] ;;< 0
    if debug? [ file-show ( list " requests pizzo from anti-pizzo member " [ who ] of selected-anti-pizzo-target ) ]
    set allowed-targets ( turtle-set selected-anti-pizzo-target allowed-targets )
  ]
  create-pay-links-from allowed-targets [ set color blue set shape "arc" set hidden? not show-extortion-links? ]
  create-threaten-links-to allowed-targets [ set color red set shape "arc" set hidden? not show-extortion-links?]
  if random-float 1.0 < propensity-to-offend-addio-pizzo and length forbidden-targets > 5
  [
    set forbidden-targets but-first forbidden-targets
  ]
end


  ;;
  ;; Plotting Procedures
  ;;

to my-update-plots


end

  ;;
  ;; Utilities
  ;;



to-report random-float-in-range [ low high ]
  report low + random-float ( high - low )
end

to-report sqr [ x ]
  report x * x
end

to-report rectify-date
  let date-string date-and-time
  let colon position ":" date-string
  set date-string replace-item  colon date-string "-"
  set colon position ":" date-string
  set date-string replace-item  colon date-string "-"
  set colon position "." date-string
  set date-string replace-item  colon date-string "-"
  report date-string
end

to save-structure [ file-name ]
  show "save-structure entered"
  let state-file-name word file-name "state.owl"
  if file-exists? state-file-name [ file-delete state-file-name ]
  owl:options "owl2"
  owl:options "relations"
  owl:options "no-patches"
  owl:domain "reports-to-links" "extorters"
  owl:domain "levies-tribute-from-links" "extorters"
  owl:domain "threaten-links" "extorters"
  owl:domain "pay-links" "targets"
  owl:range "reports-to-links" "extorters"
  owl:range "levies-tribute-from-links" "extorters"
  owl:range "threaten-links" "targets"
  owl:range "pay-links" "extorters"
  owl:model "http://userpages.uni-koblenz.de/~kgt/NOERS.owl"
  show ( list "trying to write structure to " file-name )
  set file-name "/home/ds42723/arders"
  owl:structure word file-name ".owl"
  owl:state state-file-name ticks
  show "structure seems to have been written"
end
@#$#@#$#@
GRAPHICS-WINDOW
405
15
1213
624
-1
-1
10.0
1
10
1
1
1
0
0
0
1
0
79
0
59
1
1
1
ticks
30.0

BUTTON
10
20
90
60
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
100
20
190
60
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
200
20
290
60
go once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
10
90
190
123
initial-targets
initial-targets
0
500
20.0
5
1
NIL
HORIZONTAL

SWITCH
10
675
142
708
random-seed?
random-seed?
1
1
-1000

SWITCH
10
720
140
753
debug?
debug?
1
1
-1000

TEXTBOX
25
660
85
678
World
11
0.0
1

SLIDER
10
300
165
333
extortion-level-low
extortion-level-low
0
50
25.0
2.5
1
%
HORIZONTAL

TEXTBOX
25
65
175
83
Targets' parameters:
11
0.0
1

SLIDER
210
90
390
123
initial-wealth-min
initial-wealth-min
0
50000
50000.0
1000
1
NIL
HORIZONTAL

SLIDER
210
125
390
158
initial-wealth-max
initial-wealth-max
0
100000
50000.0
1000
1
NIL
HORIZONTAL

TEXTBOX
30
455
180
473
Police's parameters:
11
0.0
1

SLIDER
10
475
185
508
initial-police
initial-police
0
100
4.0
2
1
NIL
HORIZONTAL

SLIDER
10
515
185
548
time-in-jail
time-in-jail
0
30
6.0
3
1
NIL
HORIZONTAL

PLOT
1465
345
1700
495
Mean assets of extorters
NIL
NIL
0.0
10.0
0.0
200000.0
false
true
"" ""
PENS
"active" 1.0 0 -955883 true "" "plot m-ass-ext-act"
"in jail" 1.0 0 -7500403 true "" "plot m-ass-ext-jail"
"inactive" 1.0 0 -16777216 true "" "plot m-ass-ext-inact"

PLOT
1225
345
1460
495
Mean assets of targets
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"pizzo payers" 1.0 0 -13345367 true "" "plot m-ass-non-a-p"
"addio-pizzo" 1.0 0 -2674135 true "" "plot m-ass-a-p-shops"

SLIDER
10
230
165
263
initial-extorters
initial-extorters
0
300
9.0
3
1
NIL
HORIZONTAL

SLIDER
180
300
390
333
punishment-severity-high
punishment-severity-high
0
80
50.0
10
1
%
HORIZONTAL

SLIDER
10
335
165
368
extortion-level-high
extortion-level-high
0
80
50.0
2.5
1
%
HORIZONTAL

SLIDER
180
335
390
368
punishment-severity-low
punishment-severity-low
0
60
25.0
5
1
%
HORIZONTAL

SLIDER
180
230
390
263
initial-extortion-radius
initial-extortion-radius
0
10
10.0
1
1
NIL
HORIZONTAL

SLIDER
180
265
390
298
extortion-radius-extension
extortion-radius-extension
1
2
1.1
0.1
1
NIL
HORIZONTAL

SLIDER
10
265
165
298
consumption-per-period
consumption-per-period
0
2000
150.0
50
1
NIL
HORIZONTAL

SLIDER
10
125
190
158
denunciation-propensity
denunciation-propensity
0
1
0.1
0.05
1
NIL
HORIZONTAL

SLIDER
10
555
185
588
prosecution-propensity
prosecution-propensity
0
1
0.3
0.05
1
NIL
HORIZONTAL

TEXTBOX
15
205
165
223
Extorters' parameters
11
0.0
1

SLIDER
160
675
320
708
initial-villages
initial-villages
5
50
5.0
5
1
NIL
HORIZONTAL

PLOT
1465
495
1700
645
Number of extorters
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"inactive" 1.0 0 -16777216 true "" "plot count extorters with [ not active? ]"
"in jail" 1.0 0 -7500403 true "" "plot count extorters with [ in-jail? ]"
"active" 1.0 0 -2674135 true "" "plot count extorters with [ active? and not in-jail? ]"

SLIDER
190
475
370
508
initial-public
initial-public
0
4800
180.0
20
1
NIL
HORIZONTAL

TEXTBOX
195
455
345
473
Consumers' parameters
11
0.0
1

SLIDER
190
515
370
548
addio-pizzo-threshold
addio-pizzo-threshold
0
1
0.3
0.05
1
NIL
HORIZONTAL

SWITCH
160
720
332
753
show-extortion-links?
show-extortion-links?
0
1
-1000

OUTPUT
1225
15
1700
185
8

PLOT
1465
645
1700
795
Families
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"number" 1.0 0 -16777216 true "" "plot count extorters with [ role = \"*\" and count out-levies-tribute-from-link-neighbors > 0 ]"
"max depth" 1.0 0 -8630108 true "" "plot max-family-depth"
"max size" 1.0 0 -5825686 true "" "plot size-of-largest-family"

PLOT
1225
190
1700
345
Current distribution of assets
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"addio-pizzo" 1.0 0 -2674135 true "" "plot sum [ shop-wealth ] of  targets with [ addio-pizzo? and not bankrupt? ]"
"pizzo payers" 1.0 0 -13345367 true "" "plot sum [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]"
"active extorters" 1.0 0 -955883 true "" "plot sum [ wealth ] of extorters with [ active? and not in-jail? ]"
"inactive extorters" 1.0 0 -11221820 true "" "plot sum [ wealth ] of extorters with [ not active? and not in-jail? ]"
"confiscated" 1.0 0 -16777216 true "" "plot confiscated"
"private" 1.0 0 -13840069 true "" "plot sum [ private-wealth ] of public"

MONITOR
405
655
537
700
remaining addio-pizzo
count targets with [ not bankrupt? and addio-pizzo? ]
17
1
11

MONITOR
540
655
675
700
remaining pizzo payers
count targets with [ not addio-pizzo? and not bankrupt? ]
17
1
11

MONITOR
680
655
792
700
failed addio-pizzo
count targets with [ addio-pizzo? and bankrupt? ]
17
1
11

MONITOR
795
655
907
700
failed pizzo payers
count targets with [ not addio-pizzo? and bankrupt? ]
17
1
11

MONITOR
925
655
1027
700
active extorters
count extorters with [ active? ]
17
1
11

MONITOR
1030
655
1122
700
extorters in jail
count extorters with [ in-jail? ]
17
1
11

MONITOR
1125
655
1227
700
retired extorters
count extorters with [ not active? ]
17
1
11

MONITOR
1230
655
1382
700
extorters previously in jail
count extorters with [ was-in-jail? ]
17
1
11

SLIDER
180
370
390
403
propensity-to-offend-addio-pizzo
propensity-to-offend-addio-pizzo
0
1
1.0
0.1
1
NIL
HORIZONTAL

SLIDER
10
370
165
403
reactivation-time-low
reactivation-time-low
0
20
5.0
1
1
NIL
HORIZONTAL

MONITOR
405
700
535
745
currently paying
count targets with [ addio-pizzo? and count my-out-pay-links > 0 ]
17
1
11

MONITOR
540
700
675
745
currently paying
count targets with [ not addio-pizzo? and count my-out-pay-links > 0 ]
17
1
11

MONITOR
405
745
535
790
to more than one
count targets with [ addio-pizzo? and count my-out-pay-links > 1 ]
17
1
11

MONITOR
540
745
675
790
to more than one
count targets with [ not addio-pizzo? and count my-out-pay-links > 1 ]
17
1
11

SLIDER
10
405
165
438
reactivation-time-high
reactivation-time-high
0
20
10.0
1
1
NIL
HORIZONTAL

MONITOR
855
725
935
770
total wealth
sum [ wealth ] of extorters + sum [ shop-wealth ] of targets + confiscated + total-money + sum [ private-wealth ] of public
2
1
11

MONITOR
945
725
1042
770
wealth of shops
sum [ shop-wealth ] of targets
2
1
11

MONITOR
1055
725
1165
770
wealth of extorters
sum [ wealth ] of extorters
2
1
11

MONITOR
1175
725
1252
770
confiscated
confiscated
2
1
11

SLIDER
10
160
190
193
fixed-cost-per-period
fixed-cost-per-period
0
500
300.0
100
1
NIL
HORIZONTAL

PLOT
1225
495
1460
645
Number of shops
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"pizzo-payers" 1.0 0 -13345367 true "" "plot count targets with [ not addio-pizzo? and not bankrupt? ]"
"addio-pizzo" 1.0 0 -2674135 true "" "plot count targets with [ addio-pizzo? and not bankrupt? ]"
"failed shops" 1.0 0 -7500403 true "" "plot count targets with [  bankrupt? ]"

SLIDER
190
555
370
588
initial-wealth
initial-wealth
0
1000
200.0
50
1
NIL
HORIZONTAL

SLIDER
210
160
392
193
variable-cost-coefficient
variable-cost-coefficient
0
1
0.9
0.1
1
NIL
HORIZONTAL

MONITOR
1255
725
1312
770
private
sum [ private-wealth ] of public
17
1
11

SLIDER
190
590
370
623
consumption-rate-low
consumption-rate-low
0.0
1
0.9
0.1
1
NIL
HORIZONTAL

SLIDER
190
625
370
658
consumption-rate-high
consumption-rate-high
0.0
1
1.0
0.1
1
NIL
HORIZONTAL

SWITCH
10
760
147
793
batch-version?
batch-version?
1
1
-1000

SWITCH
160
760
312
793
export-interface?
export-interface?
1
1
-1000

BUTTON
295
20
385
60
NIL
save-structure \"C:\\\\kgt\\\\Projects\\\\Current\\\\315874-GLODERS\\\\OWL\\\\owltest\"
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

@#$#@#$#@
##  WHAT IS IT?

This model is an attempt at modelling the behaviour of extorters, their victims and
police in the context of the GLODERS project. It uses some but not all of the features described by Luis Gustavo Nardin, Giulia Andrighetto, Rosaria Conte, Mario Paolucci in their IntERS model (not published yet) and extends it with territoriality and some more features as discussed during GLODERS project meetings.

This model is mainly devoted to analyse the systemic effects of the behaviour of the agents, whose decision making processes are only modelled rather crudely.

##  HOW IT WORKS

The world contains several villages (shape: church) in which several enterprises (shape: house) exist which can fall victim to extortion. Extorters (shape: person) can approach 
their targets first in their immediate vicinity, if they do not find any more potential victims in their immediate vicinity they extend this vicinity. 

**Shops** serve for providing the population with goods and services of any type. To keep things simple, these goods and services are not detailed. Shops receive payment from their customers and bear the fixed and variable costs of their business by paying into a funds which is evenly distributed to the population which is considered to provide the shops with the necessary supply (both in goods and sevrices) --- to keep things simple here, too, the households of the population receive an equal share of the overall period income of the shops, as if all of them served as suppliers and workers for the shops equally. 

Shops are often approached by criminals for "pizzo" which they can refuse at the risk of being severely punished. They can avoid this with a certain propensity to call the police (`denunciation-propensity`),  denouncing the extorters and having them prosecuted. When a shop decides to denounce an extorter it joins an "addio-pizzo" movement and makes this fact known to everybody (and to the modeller by changing its colour from blue to red).

In case they are approached by more than one extorter at the same time --- this happens mainly in the initial phase and represents what might have happened when mafia-like organisations first came into being --- the shops decide whom to pay to, and the successful extorter will then protect the shop against the rivalling extorters. When due to extortion and punishment the asset of a shop falls below zero, it is closed and does not participate in the trading process until it is compensated from a funds filled by the confiscated wealth of the extorters.

**Consumers** choose a shop for purchasing their goods. They have a certain propensity `addio-pizzo-threshold`  to choose shops belonging to the "addio-pizzo" movement. If their current shop is closed, they choose another, preferably belonging to the "addio-pizzo" movement and preferably having only a small number of customers (the reason for this is two-fold: they want to avoid crowded shops, and shops with only few customers should get a chance to prosper).

**Extorters** start their career as individual criminals and approach the nearest reachable shop, asking for a "pizzo" which is a certain proportion (either `extortion-level-LOW` or `extortion-level-HIGH` depending on the type the extorter belongs to) of its revenue per period. When the shop refuses and the police fails to successfully prosecute the extorter, the latter punishes the shop, taking away a certain proportion  of all its assets (either `punishment-severity-LOW` or `punishment-severity-HIGH`, again depending on the type the extorter belongs to). It is understood that all assets of a shop are easily convertible into money, there are no physical assets which could be destroyed. If the police hinders the extorter from punishing, the extorter is brought to jail for a certain number of periods, and all its assets are confiscated and transferred into a funds from which in turn punished shops can be compensated (following a first come--first served principle). If several extorters approach the same shop at the same time, one of them is selected by the shop to protect the shop against rivalling extorters, and the latter subordinate to the former, forming a family and eventually a hierarchy, for instance in case the successful extorter is already subordinate to someone else; if any rivalling extorters already belong to families, the family hierarchy is not changed.

**Police** try to prosecute a denounced extorter and are successful  with a certain probability (`prosecution-propensity`) --- which is their only role within the model.

## HOW TO USE IT

Set the `initial-...` sliders before pressing `setup`. This determines the numbers of the different breeds in the world. There is currently no guarantee that arbitrary combinations of the `initial-...` sliders yield reasonable results. The same applies to the sliders which determine wealth and income.

Press `setup` to populate the world with all these agents. As usual, `go` will run the simulation continuously, while `go once` will run one tick.

The `random-seed?` chooser allows to run the model with the same random seed for different parameter combinations. If it is set OFF, every run starts with a new random seed.

If the `debug?` choser is on, output will be written to the file logfile.txt. Each new run is preceded with the date and time of the start of the run unless the file is deleted. NOT TO BE USED in the applet version!

The red and blue extortion links can be switched off with the `show-extortion-links?` chooser.

The six plots show what their headlines suggest.

If the `batch-version?` choser is on, the sliders for `extortion-level-...`, `punishment-severity-...`, `prosecution-propensity`, `denunciation-propensity` and `addio-pizzo-threshold` do not work. Instead random values are assigned to these variables. This choser should only be set on in the BehaviorSpace mode.

## THINGS TO NOTICE

Target enterprises have an initial wealth uniformly distributed between the numbers on the respective min and max sliders, the income of a shop depends on the number of customers currently choosing it.

Extorters have two different extortion levels and two different severity levels (as in Nardin et al.) and a consumption per period which is currently fixed for all extorters at the same level.

Extortion is a proportion (`extortion-level`) of the income per period, punishment for not paying is a proportion (`punishment-severity`) of the total wealth of the target.  

Often the local hierarchies agglomerate to one unique hierarchy.

## THINGS TO TRY

The model can be used to find out which of the input parameters guiding the behaviour of the various kinds of agents have the greatest impact on the various output parameters (see Troitzsch 2014). It goes without saying that the model, although inspired by results of empirical research into individual behaviour done within the GLODERS project, has not yet been validated with respect to its output variables, thus it cannot be used to guide policy making, but it gives hints at which empirical macro variables to analyse.

## EXTENDING THE MODEL

The model could be extended by endowing the agents with learning capabilities. Particularly shops and extorters could then optimise their behaviour as a reaction on punishment and sanctions. Consumers could punish and sanction shops by letting shops which pay extortion know explicitly that they left them just because they did not refuse to pay pizzo. And certainly the relations between the public and the shops could be modelled in more detail, e.g. by having individual consumers work for one shop and purchase different kinds of goods from several different shops.

## NETLOGO FEATURES

Nothing special. The code uses recursive functions to determine to which family an extorter belongs and to write up the hierarchy in the output window.

## RELATED MODELS

There is one mafia model in the community (http://ccl.northwestern.edu/netlogo/models/community/Siste%20Mafia33,%20edited%202) which I have not consulted so far.

## CREDITS AND REFERENCES

The research leading to these results has received funding from the European Union Seventh Framework Programme (FP7/2007--2013) under grant agreement no. 315874 ([Global dynamics of extortion racket systems, GLODERS](http://www.gloders.eu)).   The author thanks his colleagues in this project for fruitful discussions over many years.

The model owes a lot to the Nardin et al. discussion paper and to the discussions within the GLODERS project.

## HOW TO CITE

The model and its results are documented in

Klaus G. Troitzsch (2014) Distribution Effects of Extortion Racket Systems. In: Proceedings of Artificial Economics 2014. Cham etc.: Springer 2014 (to appear in September 2014)

## COPYRIGHT NOTICE

Written by Klaus G. Troitzsch 2013-2014. The model may be used and extended if the source is quoted.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 0 60 270 150 225 240 270

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

church
false
0
Polygon -7500403 true true 105 15 75 285 105 285 135 285
Rectangle -7500403 true true 120 180 225 285
Rectangle -7500403 true true 120 180 225 285
Rectangle -7500403 true true 120 180 225 285

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

police
false
0
Circle -7500403 false true 45 45 210
Polygon -7500403 true true 96 225 150 60 206 224 63 120 236 120
Polygon -7500403 true true 120 120 195 120 180 180 180 185 113 183
Polygon -7500403 false true 30 15 0 45 15 60 30 90 30 105 15 165 3 209 3 225 15 255 60 270 75 270 99 256 105 270 120 285 150 300 180 285 195 270 203 256 240 270 255 270 285 255 294 225 294 210 285 165 270 105 270 90 285 60 300 45 270 15 225 30 210 30 150 15 90 30 75 30

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

top-down-triangle
false
0
Polygon -7500403 true true 150 270 285 45 15 45

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.4
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment 01 2014-01-07" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="50"/>
    <metric>mean [ distance-to-my-current-shop ] of public</metric>
    <metric>mean [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>mean [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ wealth ] of extorters</metric>
    <metric>confiscated</metric>
    <metric>total-money</metric>
    <metric>"##"</metric>
    <enumeratedValueSet variable="extortion-radius-extension">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-targets">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extortion-radius">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-low">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-low">
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-police">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="time-in-jail">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-public">
      <value value="1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-high">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="propensity-to-offend-addio-pizzo">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-low">
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-high">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-max">
      <value value="20000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="debug?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-min">
      <value value="20000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="denunciation-propensity">
      <value value="0.025"/>
      <value value="0.05"/>
      <value value="0.075"/>
      <value value="0.1"/>
      <value value="0.125"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-per-period">
      <value value="750"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="expenses-per-period">
      <value value="1200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-villages">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-extortion-links?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prosecution-propensity">
      <value value="0.175"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-high">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extorters">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="addio-pizzo-threshold">
      <value value="0"/>
      <value value="0.05"/>
      <value value="0.1"/>
      <value value="0.15"/>
      <value value="0.2"/>
      <value value="0.25"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment 02 2014-01-07" repetitions="20" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="50"/>
    <metric>mean [ distance-to-my-current-shop ] of public</metric>
    <metric>mean [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>mean [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ wealth ] of extorters</metric>
    <metric>confiscated</metric>
    <metric>total-money</metric>
    <metric>"##"</metric>
    <enumeratedValueSet variable="extortion-radius-extension">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-targets">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extortion-radius">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-low">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-low">
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-police">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="time-in-jail">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-public">
      <value value="1500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-high">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="propensity-to-offend-addio-pizzo">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-low">
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-high">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-max">
      <value value="20000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="debug?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-min">
      <value value="20000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="denunciation-propensity">
      <value value="0.025"/>
      <value value="0.05"/>
      <value value="0.075"/>
      <value value="0.1"/>
      <value value="0.125"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-per-period">
      <value value="750"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="expenses-per-period">
      <value value="1200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-villages">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-extortion-links?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prosecution-propensity">
      <value value="0.175"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-high">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extorters">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="addio-pizzo-threshold">
      <value value="0"/>
      <value value="0.05"/>
      <value value="0.1"/>
      <value value="0.15"/>
      <value value="0.2"/>
      <value value="0.25"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment 03 2014-01-09 13-12" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="50"/>
    <metric>mean [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>mean [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ wealth ] of extorters</metric>
    <metric>count extorters with [ role = "*" and count out-levies-tribute-from-link-neighbors &gt; 0 ]</metric>
    <metric>count targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ count my-out-pay-links &gt; 0 ]</metric>
    <metric>"##"</metric>
    <enumeratedValueSet variable="debug?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extorters">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-per-period">
      <value value="750"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-low">
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-police">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-high">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-targets">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prosecution-propensity">
      <value value="0.025"/>
      <value value="0.075"/>
      <value value="0.125"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-low">
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-high">
      <value value="30"/>
      <value value="40"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="propensity-to-offend-addio-pizzo">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-public">
      <value value="4400"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="variable-cost-coefficient">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-low">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extortion-radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-radius-extension">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="denunciation-propensity">
      <value value="0.025"/>
      <value value="0.075"/>
      <value value="0.125"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-low">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-villages">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-high">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-max">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="time-in-jail">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-high">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-min">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-cost-per-period">
      <value value="300"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="addio-pizzo-threshold">
      <value value="0"/>
      <value value="0.15"/>
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-extortion-links?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment 04 2014-01-10 16-05" repetitions="2" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="50"/>
    <metric>extortion-level-high</metric>
    <metric>punishment-severity-high</metric>
    <metric>denunciation-propensity</metric>
    <metric>prosecution-propensity</metric>
    <metric>addio-pizzo-threshold</metric>
    <metric>mean [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>mean [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ wealth ] of extorters</metric>
    <metric>count extorters with [ role = "*" and count out-levies-tribute-from-link-neighbors &gt; 0 ]</metric>
    <metric>count targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ count my-out-pay-links &gt; 0 ]</metric>
    <metric>"##"</metric>
    <enumeratedValueSet variable="debug?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extorters">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-per-period">
      <value value="750"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-low">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-police">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-high">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-targets">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prosecution-propensity">
      <value value="0.025"/>
      <value value="0.1"/>
      <value value="0.175"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-low">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-high">
      <value value="50"/>
      <value value="60"/>
      <value value="70"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="propensity-to-offend-addio-pizzo">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-public">
      <value value="4400"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="variable-cost-coefficient">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-low">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extortion-radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-radius-extension">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="denunciation-propensity">
      <value value="0.025"/>
      <value value="0.1"/>
      <value value="0.175"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-low">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-villages">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-high">
      <value value="50"/>
      <value value="60"/>
      <value value="70"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-max">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="time-in-jail">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-high">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-min">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-cost-per-period">
      <value value="300"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="addio-pizzo-threshold">
      <value value="0"/>
      <value value="0.3"/>
      <value value="0.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-extortion-links?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment 07 2014-02-06 17-15" repetitions="2000" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="50"/>
    <exitCondition>not any? targets</exitCondition>
    <metric>extortion-level-low</metric>
    <metric>extortion-level-high</metric>
    <metric>punishment-severity-low</metric>
    <metric>punishment-severity-high</metric>
    <metric>denunciation-propensity</metric>
    <metric>prosecution-propensity</metric>
    <metric>addio-pizzo-threshold</metric>
    <metric>mean [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>mean [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ wealth ] of extorters</metric>
    <metric>count extorters with [ role = "*" and count out-levies-tribute-from-link-neighbors &gt; 0 ]</metric>
    <metric>count targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ count my-out-pay-links &gt; 0 ]</metric>
    <metric>ticks</metric>
    <metric>"##"</metric>
    <enumeratedValueSet variable="initial-wealth-min">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-police">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-villages">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-max">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-targets">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extorters">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-per-period">
      <value value="750"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-low">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-high">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-low">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-high">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="time-in-jail">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-radius-extension">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="propensity-to-offend-addio-pizzo">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-cost-per-period">
      <value value="300"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="variable-cost-coefficient">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extortion-radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-public">
      <value value="4400"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-low">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-high">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-low">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-high">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="denunciation-propensity">
      <value value="0.33"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prosecution-propensity">
      <value value="0.18"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="addio-pizzo-threshold">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-extortion-links?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="debug?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="batch-version?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment 08 2014-02-09 11-15" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="52"/>
    <exitCondition>not any? targets</exitCondition>
    <metric>extortion-level-low</metric>
    <metric>extortion-level-high</metric>
    <metric>punishment-severity-low</metric>
    <metric>punishment-severity-high</metric>
    <metric>denunciation-propensity</metric>
    <metric>prosecution-propensity</metric>
    <metric>addio-pizzo-threshold</metric>
    <metric>mean [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>mean [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ wealth ] of extorters</metric>
    <metric>count extorters with [ role = "*" and count out-levies-tribute-from-link-neighbors &gt; 0 ]</metric>
    <metric>count targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ count my-out-pay-links &gt; 0 ]</metric>
    <metric>ticks</metric>
    <metric>"##"</metric>
    <enumeratedValueSet variable="initial-wealth-min">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-police">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-villages">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-max">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-targets">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extorters">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-per-period">
      <value value="750"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-low">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-high">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-low">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-high">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="time-in-jail">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-radius-extension">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="propensity-to-offend-addio-pizzo">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-cost-per-period">
      <value value="300"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="variable-cost-coefficient">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extortion-radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-public">
      <value value="4400"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-low">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-high">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-low">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-high">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="denunciation-propensity">
      <value value="0.18"/>
      <value value="0.33"/>
      <value value="0.48"/>
      <value value="0.63"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prosecution-propensity">
      <value value="0.18"/>
      <value value="0.33"/>
      <value value="0.48"/>
      <value value="0.63"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="addio-pizzo-threshold">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-extortion-links?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="debug?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="batch-version?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="export-interface?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment 07 2014-02-16 10-30" repetitions="1050" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="50"/>
    <exitCondition>not any? targets</exitCondition>
    <metric>extortion-level-low</metric>
    <metric>extortion-level-high</metric>
    <metric>punishment-severity-low</metric>
    <metric>punishment-severity-high</metric>
    <metric>denunciation-propensity</metric>
    <metric>prosecution-propensity</metric>
    <metric>addio-pizzo-threshold</metric>
    <metric>m-ass-a-p-shops ;; mean [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>m-ass-non-a-p   ;; mean [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>time-when-addio-pizzo-takes-over</metric>
    <metric>m-ass-ext-act</metric>
    <metric>m-ass-ext-jail</metric>
    <metric>m-ass-ext-inact</metric>
    <metric>sum [ shop-wealth ] of targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ shop-wealth ] of targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>sum [ wealth ] of extorters</metric>
    <metric>count extorters with [ role = "*" and count out-levies-tribute-from-link-neighbors &gt; 0 ]</metric>
    <metric>max-family-depth</metric>
    <metric>size-of-largest-family</metric>
    <metric>count targets with [ not addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ addio-pizzo? and not bankrupt? ]</metric>
    <metric>count targets with [ count my-out-pay-links &gt; 0 ]</metric>
    <metric>ticks</metric>
    <metric>"##"</metric>
    <enumeratedValueSet variable="initial-wealth-min">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-police">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-villages">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-wealth-max">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-targets">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extorters">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-per-period">
      <value value="750"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-low">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumption-rate-high">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-low">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reactivation-time-high">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="time-in-jail">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-radius-extension">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="propensity-to-offend-addio-pizzo">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-cost-per-period">
      <value value="300"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="variable-cost-coefficient">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-extortion-radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-public">
      <value value="4400"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-low">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="extortion-level-high">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-low">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment-severity-high">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="denunciation-propensity">
      <value value="0.33"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prosecution-propensity">
      <value value="0.18"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="addio-pizzo-threshold">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-extortion-links?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="random-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="debug?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="batch-version?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

arc
0.5
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

my-arc
1.0
-0.2 1 4.0 4.0
0.0 1 1.0 0.0
0.2 1 4.0 4.0
link direction
true
0
Polygon -7500403 true true 150 150 120 195 180 195 150 150
@#$#@#$#@
1
@#$#@#$#@
